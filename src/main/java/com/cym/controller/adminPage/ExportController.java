package com.cym.controller.adminPage;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Date;

import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.DownloadedFile;
import org.noear.solon.core.handle.ModelAndView;
import org.noear.solon.core.handle.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cym.config.InitConfig;
import com.cym.ext.AsycPack;
import com.cym.model.Admin;
import com.cym.service.ConfService;
import com.cym.utils.BaseController;
import com.cym.utils.JsonResult;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;

@Controller
@Mapping("/adminPage/export")
public class ExportController extends BaseController {
	Logger logger = LoggerFactory.getLogger(this.getClass());
	@Inject
	ConfService confService;

	@Mapping("")
	public ModelAndView index(ModelAndView modelAndView) {

		modelAndView.view("/adminPage/export/index.html");
		return modelAndView;
	}

	@Mapping("dataExport")
	public DownloadedFile dataExport(Context context) throws IOException {
		String date = DateUtil.format(new Date(), "yyyy-MM-dd_HH-mm-ss");

		AsycPack asycPack = confService.getAsycPack(new String[] { "all" });
		String json = JSONUtil.toJsonPrettyStr(asycPack);

		DownloadedFile downloadedFile = new DownloadedFile("application/octet-stream", new ByteArrayInputStream(json.getBytes(Charset.forName("UTF-8"))), date + ".json");
		return downloadedFile;
	}

	@Mapping(value = "dataImport")
	public void dataImport(UploadedFile file, Context context) throws IOException {
		if (file != null) {
			File tempFile = new File(homeConfig.home + "temp" + File.separator + file.name);
			file.transferTo(tempFile);
			String json = FileUtil.readString(tempFile, Charset.forName("UTF-8"));
			tempFile.delete();

			AsycPack asycPack = JSONUtil.toBean(json, AsycPack.class);
			confService.setAsycPack(asycPack);
		}
		context.redirect("/adminPage/export?over=true");
	}

	@Mapping("logExport")
	public DownloadedFile logExport(Context context) throws FileNotFoundException {
		File file = new File(homeConfig.home + "log/nginxWebUI.log");
		if (file.exists()) {
			DownloadedFile downloadedFile = new DownloadedFile("application/octet-stream", new FileInputStream(file), file.getName());
			return downloadedFile;
		}

		return null;
	}

}
